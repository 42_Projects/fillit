NAME		= fillit
CC			= gcc
S_DIR		= srcs/
O_DIR		= objs/
I_DIR		= -I includes/
FLAGS		= -Wall -Wextra -Werror $(I_DIR) $(LIBS_DIR)
FILES		= ft_all_is_placed.c ft_check_file.c ft_check_item.c ft_check_items.c ft_clear_all_from.c ft_clear_item.c ft_create_grid.c ft_error.c ft_init_tetris.c ft_insert_all.c ft_insert_item.c ft_item.c ft_parse_items.c ft_pos_for.c ft_pt_for.c ft_solve.c ft_trim_cols.c ft_trim_rows.c
FILES_FT	= ft_assign_var.c ft_atoi.c ft_bzero.c ft_display_grid.c ft_erase_to.c ft_fact.c ft_getabs.c ft_init_var.c ft_insert_at.c ft_is_char_in.c ft_isalnum.c ft_isalpha.c ft_isascii.c ft_isdigit.c ft_isprint.c ft_itoa.c ft_ln.c ft_lstadd.c ft_lstdel.c ft_lstdelone.c ft_lstiter.c ft_lstmap.c ft_lstnew.c ft_memalloc.c ft_memccpy.c ft_memchr.c ft_memcmp.c ft_memcpy.c ft_memdel.c ft_memmove.c ft_memset.c ft_point.c ft_pos_of_first_c.c ft_pow.c ft_putchar.c ft_putchar_fd.c ft_putendl.c ft_putendl_fd.c ft_putnbr.c ft_putnbr_fd.c ft_putstr.c ft_putstr_fd.c ft_size.c ft_sqrt.c ft_strcat.c ft_strchr.c ft_strclr.c ft_strcmp.c ft_strcpy.c ft_strdel.c ft_strdup.c ft_strequ.c ft_striter.c ft_striteri.c ft_strjoin.c ft_strlcat.c ft_strlen.c ft_strmap.c ft_strmapi.c ft_strncat.c ft_strncmp.c ft_strncpy.c ft_strndup.c ft_strnequ.c ft_strnew.c ft_strnstr.c ft_strrchr.c ft_strreduce.c ft_strrev.c ft_strrmc.c ft_strsplit.c ft_strstr.c ft_strsub.c ft_strtrim.c ft_strtrim_c.c ft_tolower.c ft_toupper.c
FILES_FILE	= ft_file_clear.c ft_file_close.c ft_file_getnc.c ft_file_insert.c ft_file_insert_at.c ft_file_move_by.c ft_file_move_to.c ft_file_read.c ft_file_write.c ft_open.c
LIBS_F		= ft file
SRC			= $(addprefix $(S_DIR),$(FILES))
OBJS		= $(addprefix $(O_DIR),$(FILES:.c=.o))
OBJS_FT		= $(addprefix libft/,$(FILES_FT:.c=.o))
OBJS_FILE	= $(addprefix libfile/,$(FILES_FILE:.c=.o))
LIBS		= $(addprefix -L. -l,$(LIBS_F))
RM			= rm -f

all: 	libft.a libfile.a	$(NAME)

$(NAME): 	$(OBJS) $(O_DIR)main.o
	@$(CC) $(FLAGS) $(LIBS) $^ -o $@
	@echo "./[0;34m$(NAME)[0;1m created."
	@echo "[0;1mDone."

$(O_DIR)%.o:		$(S_DIR)%.c
	@echo "[0;33m$<[0;37m"
	@echo "↪︎- [0;32m$@[0;1m"
	@$(CC) $(FLAGS) -c $< -o $@

libft.a:				$(OBJS_FT)
	@ar rc libft.a $(OBJS_FT); ranlib libft.a
	@echo "./[0;34mlibft.a[0;1m created."

libfile.a:				$(OBJS_FILE)
	@ar rc libfile.a $(OBJS_FILE); ranlib libfile.a
	@echo "./[0;34mlibfile.a[0;1m created."

libfile/%.o:		libfile/%.c
	@echo "[0;33m$<[0;37m"
	@echo "↪︎- [0;32m$@[0;1m"
	@$(CC) $(FLAGS) -c $< -o $@

libft/%.o:		libft/%.c
	@echo "[0;33m$<[0;37m"
	@echo "↪︎- [0;32m$@[0;1m"
	@$(CC) $(FLAGS) -c $< -o $@

$(O_DIR)main.o: main.c
	@echo "[0;33m$<[0;37m"
	@echo "↪︎- [0;32m$@[0;1m"
	@$(CC) $(FLAGS) -c $< -o $@

clean:
	@$(RM) $(OBJS) $(OBJS_FILE) $(OBJS_FT) objs/main.o
	@echo "[0;1mDone."

fclean: 	clean
	@$(RM) $(NAME) libft.a libfile.a
	@echo "[0;1mDone."

re:			fclean all

.PHONY: all clean fclean re
