/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 12:25:14 by fbellott          #+#    #+#             */
/*   Updated: 2015/12/27 11:49:18 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		main(int ac, char **av)
{
	t_file		*file;
	t_tetris	*tetris;

	if (ac != 2)
		return (ft_error("error\n"));
	file = ft_open(av[1], R);
	if (!file)
		return (ft_error("error\n"));
	file->read(file);
	if (!ft_check_file(file))
		return (ft_error("error\n"));
	tetris = ft_init_tetris(file);
	if (!tetris)
		return (ft_error("error\n"));
	ft_display_grid(tetris->solved);
	file->close(file);
	return (0);
}
