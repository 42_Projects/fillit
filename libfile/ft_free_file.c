/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/24 14:48:28 by fbellott          #+#    #+#             */
/*   Updated: 2015/12/27 10:11:00 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libfile.h"

char	ft_free_file(t_file *f)
{
	int	i;

	if (!f)
		return (FALSE);
	if (f->name)
		ft_strdel(&f->name);
	if (f->data)
		ft_strdel(&f->data);
	if (f->tmp)
		ft_strdel(&f->tmp);
	if (f->lines)
	{
		i = -1;
		while (f->lines[++i])
		{
			if (f->lines[i])
				ft_strdel(&f->lines[i]);
		}
		free(f->lines);
		f->lines = NULL;
	}
	free(f);
	f = NULL;
	return (TRUE);
}
