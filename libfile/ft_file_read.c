/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file_read.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 18:08:15 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/27 12:02:56 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libfile.h"

static char	*ft_fill_t_file(t_file *file, char *str)
{
	file->data = ft_strdup(str);
	file->tmp = ft_strdup(str);
	file->lines = ft_strsplit(str, '\n');
	while (file->lines[++file->nb_of_lines])
		;
	free(str);
	return (file->data);
}

char		*ft_file_read(t_file *file)
{
	int		bytes;
	char	*data;
	char	*buf;
	char	*buf_;

	if (!file || !(buf = (char *)malloc(sizeof(char) * (BUF_SIZE + 1))) ||
			!(data = (char *)malloc(sizeof(char) * 1)) ||
			!(file->access == R || file->access == RW))
		return (NULL);
	while ((bytes = read(file->fd, buf, BUF_SIZE)) > 0)
	{
		buf[bytes] = '\0';
		buf_ = ft_strdup(data);
		free(data);
		data = ft_insert_at(buf_, buf, -1);
		free(buf_);
		file->len += bytes;
		if (data[ft_strlen(data) - 1] > 46)
			return (NULL);
	}
	if (buf)
		free(buf);
	if (!data)
		return (NULL);
	return (ft_fill_t_file(file, data));
}
