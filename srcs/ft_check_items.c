/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_items.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/08 21:45:20 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/18 18:23:27 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	ft_check_items(t_tetris *t)
{
	int		i;
	int		k;
	int		item;

	if (!(item = -1) && !t)
		return (FALSE);
	while (t->items[++item] && (i = -1))
	{
		while (t->items[item]->s[++i] != '\n' && (k = -1))
			;
		while (t->items[item]->s[++k] != '#')
			;
		ft_check_item(t->items[item]->s, k, i, item);
		t->size.width += i;
		t->size.height += ft_strlen(t->items[item]->s) / (i + 1);
		t->items[item]->size.width = i;
		t->items[item]->size.height = ft_strlen(t->items[item]->s) / (i + 1);
		i = -1;
		k = 0;
		while (t->items[item]->s[++i])
			k += (t->items[item]->s[i] == 'A' + item ? 1 : 0);
		if (k != 4)
			return (FALSE);
	}
	return (TRUE);
}
