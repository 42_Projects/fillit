/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trim_rows.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/08 15:14:08 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/27 11:46:19 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	ft_trim_rows(t_tetris *t)
{
	int		i;
	int		k;
	int		item;

	if (!t)
		return (FALSE);
	k = -1;
	while (++k < 2)
	{
		item = -1;
		while (t->items[++item])
		{
			if (k == 1)
				t->items[item]->s = ft_strrev(t->items[item]->s);
			i = 0;
			while (t->items[item]->s[i++] != '#' && t->items[item]->s[i])
				;
			if (!t->items[item]->s[i])
				return (FALSE);
			t->items[item]->s += (i > 4 ? i / (5 + k) * 5 : 0);
			if (k == 1)
				t->items[item]->s = ft_strrev(t->items[item]->s);
		}
	}
	return (TRUE);
}
