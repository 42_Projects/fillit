/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_insert_item.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/09 15:22:19 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/18 18:30:21 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	ft_insert_item(t_tetris *t, int item, t_point pos)
{
	int		i;
	t_point	p;
	int		is_placed;

	i = -1;
	p.y = -1;
	if (pos.x + t->items[item]->size.width > (int)ft_strlen(t->solved[0])
		|| pos.y + t->items[item]->size.height > (int)ft_strlen(t->solved[0]))
		return (FALSE);
	is_placed = ft_insert_item_bis(t, item, pos, -3);
	if (is_placed)
		t->items[item]->pos = pos;
	if (is_placed)
		t->items[item]->is_placed = TRUE;
	return (is_placed);
}

char	ft_insert_item_bis(t_tetris *t, int k, t_point p, int i)
{
	t_point	p_;

	while (++i < 0 && (p_.y = -1) && t->items[k])
	{
		while (++p_.y < t->items[k]->size.height && (p_.x = -1))
		{
			while (++p_.x < t->items[k]->size.width)
			{
				if (i < -1)
				{
					if (t->items[k]->s[p_.y * (t->items[k]->size.width + 1) +
					p_.x] != '.' && t->solved[p_.y + p.y][p_.x + p.x] != '.')
						return (FALSE);
				}
				else
				{
					i = (t->items[k]->s[++i] == '\n' ? i + 1 : i);
					if (t->items[k]->s[i] != '.' &&
						t->solved[p_.y + p.y][p_.x + p.x] == '.')
						t->solved[p_.y + p.y][p_.x + p.x] = t->items[k]->s[i];
				}
			}
		}
	}
	return (TRUE);
}
