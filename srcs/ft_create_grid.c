/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_grid.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 21:12:06 by fbellott          #+#    #+#             */
/*   Updated: 2015/12/19 11:36:12 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		ft_create_grid(t_tetris *t, int size)
{
	int		i;
	int		j;

	if (!t)
		return (FALSE);
	if (size > t->len)
		return (FALSE);
	if (!(t->solved = (char **)malloc(sizeof(char *) * (size + 1))))
		return (FALSE);
	j = -1;
	while (++j < size && (i = -1))
	{
		if (!(t->solved[j] = (char *)malloc(sizeof(char) * (size + 1))))
			return (FALSE);
		while (++i < size)
			t->solved[j][i] = '.';
		t->solved[j][i] = '\0';
	}
	t->solved[j] = NULL;
	i = -1;
	while (t->items[++i])
		t->items[i]->is_placed = FALSE;
	return (TRUE);
}
