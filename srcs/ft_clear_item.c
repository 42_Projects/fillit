/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_clear_item.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 18:24:02 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/19 11:36:54 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		ft_clear_item(t_tetris *t, int item)
{
	t_size	s;
	t_point	p;
	t_point	p_;

	if (!t->items[item]->is_placed)
		return (FALSE);
	t->items[item]->is_placed = FALSE;
	s = t->items[item]->size;
	p = t->items[item]->pos;
	p_.y = -1;
	while (++p_.y < s.height && (p_.x = -1))
	{
		while (++p_.x < s.width)
		{
			if (t->items[item]->s[p_.y * (t->items[item]->size.width + 1) +
					p_.x] != '.')
				t->solved[p_.y + p.y][p_.x + p.x] = '.';
		}
	}
	return (TRUE);
}
