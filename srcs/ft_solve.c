/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solve.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/09 19:13:04 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/19 14:33:29 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		ft_solve(t_tetris *t)
{
	int		size;
	int		item;

	t->len = (t->size.width > t->size.height ? t->size.width : t->size.height);
	size = 1;
	while (ft_pow(size + 1, 2) < t->nb_of_items * 4)
		size++;
	while (++size <= t->len)
	{
		item = t->nb_of_items;
		while (t->items[--item])
		{
			ft_create_grid(t, size);
			ft_insert_all(t, size);
			if (t->nb_of_items == 1 && ft_all_is_placed(t))
				return (TRUE);
			if (ft_solve_bis(t, item, size, TRUE) == TRUE)
				return (TRUE);
		}
		if (ft_all_is_placed(t))
			return (TRUE);
	}
	ft_display_grid(t->solved);
	return (ft_all_is_placed(t));
}

char		ft_solve_bis(t_tetris *t, int k, int size, char flag)
{
	int		i;
	int		item;

	if (k >= t->nb_of_items)
		return (FALSE);
	ft_clear_all_from(t, k);
	i = (flag ? ft_pos_for(size, t->items[k]->pos) - 1 : -1);
	while (++i < size * (size))
	{
		if (ft_all_is_placed(t))
			return (TRUE);
		ft_clear_item(t, k);
		if (!ft_insert_item(t, k, ft_pt_for(size, i)))
			continue ;
		if (ft_all_is_placed(t))
			return (TRUE);
		item = k;
		while (t->items[++item])
		{
			if (ft_solve_bis(t, item, size, FALSE))
				return (TRUE);
		}
	}
	return (FALSE);
}
