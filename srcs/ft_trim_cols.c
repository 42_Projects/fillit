/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trim_cols.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 16:03:05 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/10 18:54:54 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		ft_trim_cols(t_tetris *tetris)
{
	int		item;
	int		k;

	if (!tetris)
		return (FALSE);
	k = 0;
	while (k < 2)
	{
		item = -1;
		while (tetris->items[++item])
		{
			if (k == 1)
				tetris->items[item]->s = ft_strrev(tetris->items[item]->s);
			ft_trim_cols_bis(tetris, item, k);
			if (k == 1)
				tetris->items[item]->s = ft_strrev(tetris->items[item]->s);
			if (k == 1)
				tetris->items[item]->s = ft_strrmc(tetris->items[item]->s, '?');
		}
		k++;
	}
	return (TRUE);
}

void		ft_trim_cols_bis(t_tetris *tetris, int item, int k)
{
	int		i;
	int		col;
	char	flag;

	i = -1;
	k = k + 0;
	tetris->items[item]->size.width = ft_strlen(tetris->items[item]->s) / 5 - 1;
	while (++i < 4)
	{
		col = tetris->items[item]->size.width;
		flag = TRUE;
		while (col >= 0)
			flag = (tetris->items[item]->s[col-- * 5 + i + k] == '#' ? 0
																	: flag);
		if (!flag)
			break ;
		col = ft_strlen(tetris->items[item]->s) / 5 - 1;
		while (col >= 0)
			tetris->items[item]->s[col-- * 5 + i + k] = '?';
	}
}
