/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 13:04:58 by fbellott          #+#    #+#             */
/*   Updated: 2015/12/16 18:11:47 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	ft_check_file(t_file *file)
{
	int		i;
	int		nb;

	if (!file)
		return (FALSE);
	i = 0;
	nb = 0;
	while (file->tmp[i])
	{
		while ((i - nb) % 5 < 4 && file->tmp[i])
		{
			if (file->tmp[i] != '.' && file->tmp[i] != '#')
				return (FALSE);
			i++;
		}
		if (file->tmp[i++] != '\n')
			return (FALSE);
		if (i > 0 && i % 21 == 20 && i < (int)ft_strlen(file->tmp))
		{
			if (file->tmp[i++] != '\n')
				return (FALSE);
			nb++;
		}
	}
	return ((i != ((nb + 1) * 21 - 1) ? FALSE : (nb + 1)));
}
