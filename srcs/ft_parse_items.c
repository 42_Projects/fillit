/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_items.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 22:50:04 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/11 13:18:27 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	ft_parse_items(t_tetris *tetris, t_file *file)
{
	int		i;
	int		j;
	int		k;

	if (!file || !tetris)
		return (FALSE);
	ft_assign_var(&i, &k, 0, 0);
	tetris->items = (t_item **)malloc(sizeof(t_item *)
										* (tetris->nb_of_items + 1));
	if (!tetris->items)
		return (FALSE);
	i = 0;
	while (i < (tetris->nb_of_items * 4))
	{
		j = 0;
		tetris->items[k] = ft_item(k);
		if (!tetris->items[k])
			return (FALSE);
		while (j++ < 4)
			tetris->items[k]->s = ft_insert_at(tetris->items[k]->s,
								ft_insert_at(file->lines[i++], "\n", 5), 20);
		k += (i % 4 == 0 ? 1 : 0);
	}
	tetris->items[k] = NULL;
	return (TRUE);
}
