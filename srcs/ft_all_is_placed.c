/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_all_is_placed.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 21:10:38 by fbellott          #+#    #+#             */
/*   Updated: 2015/12/14 21:11:46 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		ft_all_is_placed(t_tetris *t)
{
	int		item;

	item = -1;
	while (t->items[++item])
	{
		if (!t->items[item]->is_placed)
			return (FALSE);
	}
	return (TRUE);
}
