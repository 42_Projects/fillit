/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_item.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 18:17:32 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/18 18:23:15 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_check_item(char *s, int pos, int cols, char item)
{
	if (!s[pos])
		return ;
	if (s[pos] == '#')
		s[pos] = 'A' + item;
	if (s[pos + 1] == '#')
		ft_check_item(s, pos + 1, cols, item);
	if (s[pos + cols + 1] == '#')
		ft_check_item(s, pos + cols + 1, cols, item);
	if (s[pos - 1] == '#')
		ft_check_item(s, pos - 1, cols, item);
}
