/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_clear_all_from.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 18:31:32 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/18 18:32:28 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_clear_all_from(t_tetris *t, int item)
{
	while (t->items[item])
		ft_clear_item(t, item++);
}
