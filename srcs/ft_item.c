/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_item.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/10 15:24:34 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/19 11:37:09 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_item		*ft_item(void)
{
	t_item	*item;

	item = (t_item *)malloc(sizeof(t_item));
	if (!item)
		return (NULL);
	item->s = (char *)malloc(sizeof(char) * (20 + 1));
	if (!item->s)
		return (NULL);
	item->s[0] = '\0';
	item->size.width = 0;
	item->size.height = 0;
	return (item);
}
