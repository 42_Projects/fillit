/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_insert_all.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 18:32:54 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/18 18:36:14 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_insert_all(t_tetris *t, int size)
{
	int	i;
	int	item;

	item = -1;
	while (t->items[++item] && (i = -1))
	{
		while (!ft_insert_item(t, item, ft_pt_for(size, ++i)) &&
					i < size * size)
			;
	}
}
