/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_tetris.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 22:58:49 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/18 18:23:42 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_tetris	*ft_init_tetris(t_file *file)
{
	t_tetris	*tetris;

	if (!file)
		return (NULL);
	tetris = (t_tetris *)malloc(sizeof(t_tetris));
	if (!tetris)
		return (NULL);
	if (!(tetris->nb_of_items = ft_check_file(file)))
		return (NULL);
	if (!ft_parse_items(tetris, file))
		return (NULL);
	if (!ft_trim_rows(tetris) || !ft_trim_cols(tetris))
		return (NULL);
	if (!ft_check_items(tetris))
		return (NULL);
	if (!ft_solve(tetris))
		return (NULL);
	return (tetris);
}
