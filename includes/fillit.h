/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 12:29:42 by fbellott          #+#    #+#             */
/*   Updated: 2015/12/19 11:41:51 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "libft.h"
# include "libfile.h"

typedef struct	s_item
{
	char		*s;
	t_size		size;
	t_point		pos;
	char		is_placed;
}				t_item;

typedef struct	s_tetris
{
	t_item		**items;
	char		**solved;
	int			nb_of_items;
	t_size		size;
	int			len;
	int			item;
}				t_tetris;

t_tetris		*ft_init_tetris(t_file *file);
t_item			*ft_item();
char			ft_parse_items(t_tetris *tetris, t_file *file);
char			ft_trim_rows(t_tetris *tetris);
char			ft_trim_cols(t_tetris *tetris);
void			ft_trim_cols_bis(t_tetris *tetris, int item, int k);
char			ft_solve(t_tetris *tetris);
char			ft_solve_bis(t_tetris *tetris, int k, int size, char flag);
char			ft_create_grid(t_tetris *tetris, int size);

/*
**	Checks
*/
char			ft_check_file(t_file *file);
char			ft_check_items(t_tetris *tetris);
void			ft_check_item(char *s, int pos, int cols, char item);

/*
**	Insert
*/
char			ft_insert_item(t_tetris *tetris, int item, t_point pos);
char			ft_insert_item_bis(t_tetris *t, int item, t_point pos, int k);
void			ft_insert_all(t_tetris *t, int size);

/*
**	Clear
*/
char			ft_clear_item(t_tetris *t, int item);
void			ft_clear_all_from(t_tetris *t, int item);

/*
**	Stuff
*/
t_point			ft_pt_for(int size, int pos);
int				ft_pos_for(int size, t_point p);
char			ft_all_is_placed(t_tetris *tetris);
int				ft_error(char *msg);

#endif
